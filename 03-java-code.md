# Chronicles of Aethelgard
## Little "RAG" demo with LangChain4J, Ollama and Vert-x
>Java Docker GenAI Stack ☕️🐳🤖🦜🔗🦙

## Java code (the important parts)

### Embedding creation
```java
// ----------------------------------------
// 📝 Load the document to use it for RAG
// ----------------------------------------
File directoryPath = new File(System.getProperty("user.dir")+"/documents");
TextDocumentParser documentParser = new TextDocumentParser();
Document document = loadDocument(directoryPath.toPath()+"/rules.md", documentParser);

// ----------------------------------------
// 📝📝📝 Create chunks from the document
// ----------------------------------------
DocumentSplitter splitter = DocumentSplitters.recursive(1536, 128);
List<TextSegment> segments = splitter.split(document);

// ----------------------------------------
// 📙 Create embeddings
// ----------------------------------------
EmbeddingModel embeddingModel = new BgeSmallEnV15QuantizedEmbeddingModel();
// BgeSmallEnV15QuantizedEmbeddingModel is an in-process embedding model

List<Embedding> embeddings = embeddingModel.embedAll(segments).content();

// ----------------------------------------
// 📦 Store embeddings in a vector db
// ----------------------------------------
EmbeddingStore<TextSegment> embeddingStore = new InMemoryEmbeddingStore<>();
embeddingStore.addAll(embeddings, segments);


// 🔎 The content retriever is responsible for retrieving relevant content based on a user query.
ContentRetriever contentRetriever = EmbeddingStoreContentRetriever.builder()
    .embeddingStore(embeddingStore)
    .embeddingModel(embeddingModel)
    .maxResults(10) // on each interaction we will retrieve the 10 most relevant segments
    .minScore(0.8) // we want to retrieve segments at least somewhat similar to user query
    .build();

```

### Similarity search and completion
```java

router.post("/prompt").handler(ctx -> {

    var question = ctx.body().asJsonObject().getString("question");
    var systemContent = ctx.body().asJsonObject().getString("system");

    // ----------------------------------------
    // 📝 Prepare message for the prompt
    // ----------------------------------------
    SystemMessage systemInstructions = systemMessage(systemContent);
    UserMessage humanMessage = UserMessage.userMessage(question);

    // ----------------------------------------
    // 🔎 search similarities in the vector db
    // ----------------------------------------
    var similarities = contentRetriever.retrieve(new Query(question));

    // ----------------------------------------
    // 📝📝📝 Create context with similarities
    // ----------------------------------------
    StringBuilder content = new StringBuilder();
    content.append("<content>");
    similarities.forEach(similarity -> {
    content.append("<doc>"+similarity.toString()+"</doc>");
    });
    content.append("</content>");

    SystemMessage contextMessage = systemMessage(content.toString());

    // ----------------------------------------
    // 📝 Create the prompt
    // ----------------------------------------
    List<ChatMessage> messages = new ArrayList<>();
    messages.add(systemInstructions);
    messages.addAll(memory.messages());
    messages.add(contextMessage);
    messages.add(humanMessage);

    // ----------------------------------------
    // 🧠 Update the memory of the conversation
    // ----------------------------------------
    memory.add(humanMessage);

    HttpServerResponse response = ctx.response();

    response
        .putHeader("Content-Type", "application/octet-stream")
        .setChunked(true);

    // ----------------------------------------
    // 🤖 Generate the completion (stream)
    // ----------------------------------------
    streamingModel.generate(messages, new StreamingResponseHandler<AiMessage>() {
        @Override
        public void onNext(String token) {
            if (cancelRequest) {
                cancelRequest = false;
                throw new RuntimeException("🤬 Stop!");
            }

            System.out.println("New token: '" + token + "'");
            response.write(token);
        }

        @Override
        public void onComplete(Response<AiMessage> modelResponse) {
            memory.add(modelResponse.content());
            System.out.println("Streaming completed: " + modelResponse);
            response.end();
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }
    });

});

```