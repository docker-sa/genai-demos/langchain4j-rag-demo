import { html, render, Component } from '../js/preact-htm.js'

import Tools from './libs/Tools.js' 

// Default values
//const human_message = `Who are the players of Chronicles of Aethelgard? Give details for every player.`
      
const human_message = `Who are all the monsters of Chronicles of Aethelgard?`
      
const system_message = `You are the dungeon master, 
expert at interpreting and answering questions based on provided sources.
Using the provided context, answer the user's question 
to the best of your ability using only the resources provided. 
Be verbose!`


class Form extends Component {

    constructor(props) {
        super()

        this.state = {
            systemInstructions: system_message,
            userQuestion: human_message,
            aborter: new AbortController()
        }

        this.userQuestionOnChange = this.userQuestionOnChange.bind(this)
        this.userQuestionOnInput = this.userQuestionOnInput.bind(this)

        this.systemInstructionsOnChange = this.systemInstructionsOnChange.bind(this)
        this.systemInstructionsOnInput = this.systemInstructionsOnInput.bind(this)


        this.btnSubmitPromptOnClick = this.btnSubmitPromptOnClick.bind(this)

        this.btnClearAnswerOnClick = this.btnClearAnswerOnClick.bind(this)

        this.btnStopOnClick = this.btnStopOnClick.bind(this)

        this.btnClearHistoryOnClick = this.btnClearHistoryOnClick.bind(this)
        this.btnPrintMemoryOnClick = this.btnPrintMemoryOnClick.bind(this)

        this.btnClearUserQuestionOnClick = this.btnClearUserQuestionOnClick.bind(this)
    }

    componentDidMount() {

    }

    register(answer) {
        console.log("Register", answer)
        this.setState({
            answer: answer
        })
    }

    userQuestionOnChange(e) {
        console.log(this.state.userQuestion)
    }

    userQuestionOnInput(e) {
        this.setState({ userQuestion: e.target.value })
    }

    systemInstructionsOnChange(e) {
        console.log(this.state.systemInstructions)
    }

    systemInstructionsOnInput(e) {
        this.setState({ systemInstructions: e.target.value })
    }

    async btnSubmitPromptOnClick(e) {

        let answer = this.state.answer
        let question = this.state.userQuestion
        let aborter = this.state.aborter
        let system = this.state.systemInstructions

        Tools.completion(system, question, answer, aborter, this)
    }

    btnClearAnswerOnClick(e) {
        this.setState({
            userQuestion: ""
        })
        this.state.answer.changeResponse("")
        this.state.answer.changeMessageHeader("")

    }

    btnStopOnClick(e) {
        this.state.aborter.abort()
    }

    async btnClearHistoryOnClick(e) {
        try {
            const response = await fetch("/clear-history", {
              method: "DELETE",
            })
            console.log(response)
          } catch(error) {
            console.log("😡", error)
          }
    }

    btnPrintMemoryOnClick(e) {
        console.log("hello")
        fetch('/message-history', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
            },
          })
          .then(response => response.json())
          .then(response => console.log(response))
    }

    btnClearUserQuestionOnClick(e) {
        this.setState({
            userQuestion: ""
        })
    }

    render() {
        return html`
        <!-- system and question -->
        <div class="container">

            <div class="field">
                <label class="label">System</label>

                <div class="control">
                    <textarea 
                        class="textarea is-primary" 
                        rows="5" 
                        placeholder="System message"
                        value=${this.state.systemInstructions} 
                        onInput=${this.systemInstructionsOnInput}
                        onChange=${this.systemInstructionsOnChange}>
                    </textarea>
                </div>
            </div>


            <div class="field">
                <label class="label">Question</label>

                <div class="control">
                    <textarea 
                        class="textarea is-primary" 
                        rows="5" 
                        placeholder="Type your question here"
                        value=${this.state.userQuestion} 
                        onInput=${this.userQuestionOnInput}
                        onChange=${this.userQuestionOnChange}>
                    </textarea>
                </div>
            </div>

            <!-- buttons bar -->
            <div class="content">
                <div class="field is-grouped">
    
                    <div class="control">
                        <button 
                            class="button is-link is-small"
                            onclick=${this.btnSubmitPromptOnClick}>Submit</button>
                    </div>

                    <div class="control">
                        <button 
                            class="button is-link is-info is-small"
                            onclick=${this.btnClearUserQuestionOnClick}>Clear User Question</button>
                    </div>
                
                    <div class="control">
                        <button 
                            class="button is-link is-warning is-small"
                            onclick=${this.btnClearHistoryOnClick}>Clear the conversation summary</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-link is-info is-small"
                            onclick=${this.btnClearAnswerOnClick}>Clear All</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-link is-danger is-small"
                            onclick=${this.btnStopOnClick}>Stop</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-success is-small"
                            onclick=${this.btnPrintMemoryOnClick}>Print conversation summary to the console</button>
                    </div>
    
                </div>
            </div>
            <!-- buttons bar -->

        </div>
        <!-- system and question -->

        `
    }
}

export default Form


