import { html, render, Component } from '../js/preact-htm.js'

class Answer extends Component {

    setRefResponse = (dom) => this.response = dom

    constructor(props) {
        super()

        this.state = {
            message: "...",
            response: "..."
        }
    }

    changeMessageHeader(text) {
        this.setState({
            message: text
        })
    }

    changeResponse(text) {
        this.setState({
            response: text
        })
        this.response.innerHTML = text
    }

    render() {
        return html`
        <!-- answer (result) -->
        <div class="container">

            <div class="content">
                <article class="message is-dark">
                    <div class="message-header">
                        <p id="msg_header">🤖 Answer: ${this.state.message}</p>
                    </div>

                    <div ref=${this.setRefResponse} id="txt_response" class="message-body">
                    </div>

                </article>
            </div>

        </div>
        <!-- answer (result) -->
        `
    }
}

export default Answer


