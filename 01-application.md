# Chronicles of Aethelgard
## Little "RAG" demo with LangChain4J, Ollama and Vert-x
>Java Docker GenAI Stack ☕️🐳🤖🦜🔗🦙



## Application architecture

<img src="./imgs/archi.drawio.png" alt="drawing" width="50%"/>

[`rules.md`](./documents/rules.md) is a document that will be used to create the embeddings. It is a markdown file that contains the rules of a role playing game. 

The document is split into chunks of 1536 characters with an overlap of 128 characters. The embeddings are created using the `BgeSmallEnV15QuantizedEmbeddingModel` model (In-process bge-small-en-v1.5 embedding model). The embeddings are stored in an in-memory database. 
